import * as React from "react";
import {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useState,
} from "react";
import { BlockDigitTime } from "./styled/BlockDigitTime";
import { BlockDigitTimeContainer } from "./styled/BlockDigitTimeContainer";
import { Center } from "./styled/Center";
import { Container as ContainerClock } from "./styled/Container";
import { Harrow } from "./styled/Harrow";
import { MainContainer } from "./styled/MainContainer";
import { Marrow } from "./styled/Marrow";
import { Sarrow } from "./styled/Sarrow";
import { Section } from "./styled/Section";

const timeValues = [
  "I",
  "II",
  "III",
  "IV",
  "V",
  "VI",
  "VII",
  "VIII",
  "IX",
  "X",
  "XI",
  "XII",
];

interface ClockProps {
  offset: number;
}

const _Clock = (props: ClockProps) => {
  const offset = props.offset;
  const [time, setTime] = useState<Date>(
    new Date(
      new Date().getTime() +
        new Date().getTimezoneOffset() * 60 * 1000 +
        offset * 60 * 60 * 1000
    )
  );

  const intervalTimer = useCallback(
    () =>
      setInterval(() => {
        setTime(
          new Date(
            new Date().getTime() +
              new Date().getTimezoneOffset() * 60 * 1000 +
              offset * 60 * 60 * 1000
          )
        );
      }, 1 * 100),
    [offset]
  );

  useEffect(() => {
    let interval = intervalTimer();
    return () => {
      if (!!interval) clearInterval(interval);
    };
  }, [offset]);
  return (
    <MainContainer>
      <ContainerClock>
        {timeValues.map((e, i) => (
          <Section
            style={{ transform: `rotate(${30 * (i + 1)}deg)` }}
            key={e}
            value={e}
          />
        ))}
        <div />
        <Center />
        <Sarrow sec={time.getSeconds()} />
        <Harrow h={time.getHours()} />
        <Marrow m={time.getMinutes()} />
      </ContainerClock>
      <BlockDigitTimeContainer>
        <BlockDigitTime>{time.getHours()}</BlockDigitTime>
        <span>:</span>
        <BlockDigitTime>{time.getMinutes()}</BlockDigitTime>
        <span>:</span>
        <BlockDigitTime>{time.getSeconds()}</BlockDigitTime>
      </BlockDigitTimeContainer>
    </MainContainer>
  );
};
_Clock.defaultProps = {
  offset: 0,
};
export const Clock = _Clock;
