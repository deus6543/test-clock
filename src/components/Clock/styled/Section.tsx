import * as React from "react";
import styled, { CSSProperties } from "styled-components";

const Div = styled.div`
  text-align: center;
  width: 100%;
  height: 100%;
  position: absolute;
  font-size: 70%;
`;

interface SectionProps {
  value: string;
  style?: CSSProperties;
}

export const Section = (props: SectionProps) => (
  <Div style={props.style}>{props.value}</Div>
);
