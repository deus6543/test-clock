import styled from "styled-components";
export const Center = styled.div`
  position: absolute;
  width: 5%;
  height: 5%;
  background: white;
  border-radius: 50%;
  z-index: 11;
`;
