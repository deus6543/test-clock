import * as React from "react";
import styled, { css, CSSProperties } from "styled-components";

const Container = styled.div<{ sec: number }>`
  width: 100%;
  height: 90%;
  position: absolute;
  z-index: 10;
  ${(props) =>
    typeof props.sec !== "undefined" &&
    css`
      transform: rotate(${props.sec * 6}deg);
    `}
  &:before {
    content: "";
    position: absolute;
    height: 50%;
    width: 4px;
    background: #ff7c7c;
    left: calc(50% - 2px);
    border-radius: 2px;
  }
`;
interface SarrowProps {
  sec?: number;
}

// @ts-ignore
export const Sarrow = (props: SarrowProps) => <Container sec={props.sec} />;
