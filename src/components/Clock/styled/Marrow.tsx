import * as React from "react";
import styled, { css } from "styled-components";

const Container = styled.div<{ m: number }>`
  width: 100%;
  height: 80%;
  position: absolute;
  z-index: 1;
  ${(props) =>
    typeof props.m != "undefined" &&
    css`
      transform: rotate(${props.m * 6}deg);
    `}
  &:before {
    content: "";
    position: absolute;
    height: 50%;
    width: 6px;
    background: black;
    left: calc(50% - 3px);
    border-radius: 3px;
  }
`;
interface SarrowProps {
  m?: number;
}

// @ts-ignore
export const Marrow = (props: SarrowProps) => <Container m={props.m} />;
