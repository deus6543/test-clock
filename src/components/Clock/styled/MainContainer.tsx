import styled from "styled-components";
export const MainContainer = styled.div`
  & * {
    box-sizing: border-box;
  }
  width: 200px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
  margin-bottom: 10px;
`;
