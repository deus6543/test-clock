import * as React from "react";
import styled, { css } from "styled-components";

const Container = styled.div<{ h: number }>`
  width: 100%;
  height: 70%;
  position: absolute;
  z-index: 1;
  ${(props) =>
    typeof props.h !== "undefined" &&
    css`
      transform: rotate(${props.h * 30}deg);
    `}
  &:before {
    content: "";
    position: absolute;
    height: 50%;
    width: 8px;
    background: black;
    left: calc(50% - 4px);
    border-radius: 4px;
  }
`;

interface HarrowProps {
  h?: number;
}

// @ts-ignore
export const Harrow = (props: HarrowProps) => (
  <Container h={props.h && props.h >= 12 ? props.h - 12 : props?.h || 0} />
);
