import styled from "styled-components";

export const Container = styled.div`
  height: 200px;
  border-radius: 50%;
  width: 200px;
  background: #baaeae;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`;
