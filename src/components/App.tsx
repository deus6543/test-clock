/** @format */

import * as React from "react";
import { Clock } from "./Clock";
import { ControlClock } from "./ControlClock";

export const App = () => (
  <div>
    <ControlClock />
  </div>
);
