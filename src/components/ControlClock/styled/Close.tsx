import styled from "styled-components";

export const Close = styled.span`
  cursor: pointer;
`;
