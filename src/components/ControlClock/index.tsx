import * as React from "react";
import { useEffect, useState } from "react";
import { Clock } from "../Clock";
import { Label } from "./Label";
import { Control } from "./MainContainer";
import { ClockContainer } from "./styled/ClockContainer";
import { Close } from "./styled/Close";
interface ParamsClock {
  offset: number;
}

export const ControlClock = () => {
  const [input, setInput] = useState<number>(0);
  const [paramsClock, setParamsClock] = useState<ParamsClock[]>([]);

  const setParamsClockLocalStorage = (value: typeof paramsClock) => {
    localStorage.setItem("paramsClock", JSON.stringify(value));
    setParamsClock(value);
  };
  useEffect(() => {
    const paramsClock = localStorage.getItem("paramsClock");
    if (paramsClock) setParamsClock(JSON.parse(paramsClock));
  }, []);

  return (
    <>
      <ClockContainer>
        {paramsClock.map((e, i) => (
          <div
            key={i}
            style={{
              marginRight: 30,
              flexBasis: "200px",
              flexGrow: 1,
              maxWidth: 200,
            }}
          >
            <Close
              onClick={() =>
                setParamsClockLocalStorage(
                  paramsClock.filter((e, ip) => ip !== i)
                )
              }
            >
              X
            </Close>
            <Clock offset={e.offset} />
            <div>
              <Label>Cмещение по часовому поясу:</Label>
            </div>
            <input
              style={{ width: 40 }}
              type="number"
              onChange={(e) => {
                const changeParamsClock = [
                  ...paramsClock.map((e) => ({ ...e })),
                ];
                changeParamsClock[i].offset = parseInt(e.target.value, 10);
                setParamsClockLocalStorage(changeParamsClock);
              }}
              value={e.offset}
            />
          </div>
        ))}
      </ClockContainer>
      <Control>
        <Label>Cмещение по часовому поясу:</Label>
        <input
          type={"number"}
          value={input}
          onChange={(e) => setInput(parseInt(e.target.value, 10))}
        />
        <button
          onClick={() => {
            setParamsClockLocalStorage([...paramsClock, { offset: input }]);
            setInput(0);
          }}
        >
          Создать
        </button>
      </Control>
    </>
  );
};
